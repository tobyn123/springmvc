package com.mymvc.mymvc.domain;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.List;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PostRepoTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private PostRepo P_REPO;



    @Test
    public void whenfindAll() {


        Post post1 = new Post();
        post1.setContent("TestContent");
        post1.setTitle("TestTitle");
        entityManager.persist(post1);
        entityManager.flush();

        //when
        List<Post> all = P_REPO.findAll();

        //then
        assertThat(all.size()).isEqualTo(3);
        assertThat(all.get(2)).isEqualTo(post1);



    }

    @Test
    public void whenfindPostById() {
        Post post1 = new Post();
        post1.setContent("TestContent");
        post1.setTitle("TestTitle");
        entityManager.persist(post1);
        entityManager.flush();

        Post testPost = P_REPO.findPostById(post1.getId());

        //then
        assertThat(testPost.getContent()).isEqualTo(post1.getContent());

    }
}