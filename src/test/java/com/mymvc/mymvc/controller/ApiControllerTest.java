package com.mymvc.mymvc.controller;
import com.mymvc.mymvc.service.PostService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ApiControllerTest {

    @Autowired
    private PostService MOCK_SERV;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void getAllRecipes_ShouldReturnAllRecipes() throws Exception {

    }

    @Test
    public void findRecipeById() {

    }

    @Test
    public void deleteRecipeById() {

    }

    @Test
    public void addComment() {

    }

    @Test
    public void updateComment() {

    }
}