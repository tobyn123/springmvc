package com.mymvc.mymvc.domain;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Null;

@Entity
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer id;

    @NotEmpty
    @Column (name = "name")
    public String name;

    @NotEmpty
    @Column (name = "email")
    public String email;

    @NotEmpty
    @Column (name = "password")
    public String password;

    @NotEmpty
    @Column (name = "type")
    public String type;

    @Null
    @Column (name = "api_key")
    public String api;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getType() {
        return type;
    }

    public String getApi() {
        return api;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setApi(String api) {
        this.api = api;
    }

}
