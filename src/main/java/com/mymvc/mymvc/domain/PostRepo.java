package com.mymvc.mymvc.domain;


import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepo extends CrudRepository<Post, Integer> {

    @Override
    List<Post> findAll();

    Post findPostById (Integer id);
}
