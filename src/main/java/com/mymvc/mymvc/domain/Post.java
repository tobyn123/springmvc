package com.mymvc.mymvc.domain;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer id;

    @NotEmpty(message = "This field must contain a recipe name.")
    @Column(name = "title")
    @Size(min=5,max=50, message="Title must be between 5 and 50 characters in length.")
    public String title;

    @NotEmpty(message = "This field must contain a recipe.")
    @Column(name = "content")
    @Size(min=10,max=255, message="Recipe must be between 10 and 255 characters in length.")
    public String content;

    public Integer getId() {
        return id;
    }

    @Column(name = "date_posted")
    public String date_posted;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate_posted() {
        return date_posted;
    }


}
