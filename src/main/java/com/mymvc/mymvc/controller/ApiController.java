package com.mymvc.mymvc.controller;

import com.mymvc.mymvc.service.PostService;
import com.mymvc.mymvc.domain.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api")
@RestController
public class ApiController {
    @Autowired
    private PostService P_SERV;

    @GetMapping("/recipes")
    public List<Post> getAllRecipes() {
        return P_SERV.findAllPosts();
    }

    @GetMapping("/recipe/{id}")
    public Post findRecipeById (@PathVariable("id") Integer id ){

        return this.P_SERV.findPostById(id);

    }

    @DeleteMapping("/recipe/{id}")
    public void deleteRecipeById (@PathVariable("id") Integer id){
        this.P_SERV.deleteRecipeById(id);
    }

    @PostMapping("/add")
    public void addComment(@RequestBody Post post){
        this.P_SERV.save(post);
    }

    @PostMapping("/update")
    public void updateComment(@RequestBody Post post){
        this.P_SERV.save(post);
    }
}
