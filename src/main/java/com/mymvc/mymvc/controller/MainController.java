package com.mymvc.mymvc.controller;


import com.mymvc.mymvc.domain.User;
import com.mymvc.mymvc.domain.Post;
import com.mymvc.mymvc.service.PostService;
import com.mymvc.mymvc.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;


@Controller
public class MainController {

    private static final String HOME_TEMPLATE = "home";
    private static final String POST_TEMPLATE = "post";
    private static final String POST_TEMPLATE_HEADER = "PostTitle";
    private static final String POST_ENTRIES = "posts";
    private static final String POST_ENTRY = "post";
    private static final String NEW_POST_TEMPLATE = "newPost";
    private static final String HOMEPAGE_REDIRECT = "redirect:/";
    private static final String RECIPE_FORM_HEADER_ID = "formheader";

    @Autowired
    private UserService U_SERV;

    @Autowired
    private PostService P_SERV;

    @GetMapping("/")
    public String displayHome(Model model) {
        model.addAttribute(RECIPE_FORM_HEADER_ID, "+Recipe");
        model.addAttribute("title", "Welcome");
        model.addAttribute(POST_ENTRIES, this.P_SERV.findAllPosts());

        model.addAttribute(NEW_POST_TEMPLATE, new Post());

        return HOME_TEMPLATE;

    }

    @PostMapping("/")
    public String addComment(Model model, @Valid @ModelAttribute(NEW_POST_TEMPLATE) Post newPost, BindingResult bindingResult) {
        if (!bindingResult.hasErrors()) {
            this.P_SERV.save(newPost);

            return HOMEPAGE_REDIRECT;
        } else {

            model.addAttribute(POST_ENTRIES, this.P_SERV.findAllPosts());
            return HOME_TEMPLATE;
        }

    }

    @GetMapping("/view/{id}")
    public String displayPost(@PathVariable Integer id, Model model) {

        model.addAttribute(POST_ENTRY, this.P_SERV.findPostById(id));
        model.addAttribute(POST_TEMPLATE_HEADER, "View a post:");

        return POST_TEMPLATE;
    }

    @GetMapping("/delete/{id}")
    public String deleteComment(@PathVariable Integer id) {
        this.P_SERV.deleteRecipeById(id);
        return HOMEPAGE_REDIRECT;
    }

    @GetMapping("update/{id}")
    public String editComment(Model model, @PathVariable Integer id) {
        model.addAttribute("title", "Edit your recipe by clicking the button above.");
        model.addAttribute(POST_ENTRIES, this.P_SERV.findAllPosts());
        model.addAttribute(RECIPE_FORM_HEADER_ID, "Edit Recipe");
        model.addAttribute(NEW_POST_TEMPLATE, this.P_SERV.findPostById(id));
        return HOME_TEMPLATE;
    }

    @PostMapping("update/{id}")
    public String saveComment(Model model,
                              @PathVariable Integer id,
                              @Valid @ModelAttribute(NEW_POST_TEMPLATE) Post newPost,
                              BindingResult bindingResult) {

        if (!bindingResult.hasErrors()) {
            Post current = this.P_SERV.findPostById(id);
            current.setTitle(newPost.getTitle());
            current.setContent(newPost.getContent());
            this.P_SERV.save(current);

            return HOMEPAGE_REDIRECT;
        } else {
            model.addAttribute(RECIPE_FORM_HEADER_ID, "Please Correct the Comment");
            model.addAttribute(NEW_POST_TEMPLATE, this.P_SERV.findAllPosts());
            return HOME_TEMPLATE;
        }

    }
}
