package com.mymvc.mymvc.service;

import com.mymvc.mymvc.domain.Post;
import com.mymvc.mymvc.domain.PostRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
    @Autowired
    private PostService P_SERV;

    @Autowired
    private PostRepo P_REPO;

    public List<Post> findAllPosts(){

        return this.P_REPO.findAll();
    }

    public Post findPostById(Integer id){
        return this.P_REPO.findPostById(id);
    }

    public void save(Post post){
        this.P_REPO.save(post);
    }

    public void deleteRecipeById(Integer id) {
        this.P_REPO.delete(findPostById(id));
    }
}
