package com.mymvc.mymvc.service;

import com.mymvc.mymvc.domain.User;
import com.mymvc.mymvc.domain.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserService U_SERV;

    @Autowired
    private UserRepo U_REPO;

    public List<User> findAllUsers(){
        return this.U_REPO.findAll();
    }

    public void save(User user) {

        this.U_REPO.save(user);

    }

}
